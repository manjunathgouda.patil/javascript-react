import React from "react"
import Navbar from "./Navbar"
import Hero from "./Hero"
import image1 from "./asset/image1.jpg"
//import image2 from "./asset/image2.jpg"
//import image3 from "./asset/image3.jpg"
import Star from "./asset/Star.jpg"
import Card from "./Card"
import data from "./data"
export default function App(){
    const cards=data.map(item=>{
        return(
            <Card
            image={image1}
            title={item.title}
            rating={item.rating}
            star={Star}
            num={item.num}
            cty={item.cty}
            price={item.price}
            />
        )
    })
    return(
        <div>
            <Navbar/>
            <Hero/>
            {cards}
        </div>
    )
}
