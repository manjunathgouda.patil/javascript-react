import React from "react"
import image from "./asset/image1.jpg"
import Star from "./asset/Star.jpg"
export default function Card(props)
{
    console.log(props)
    return(
        <div className="Card">
           <img src={image} className="Cardimage"/>
           <div className="Stats">
               <img src={Star} className="Star"/>
               <span>{props.rating}</span>
               <span>{props.num} </span>
               <span>{props.cty}</span>
           </div>
           <p>{props.title}</p>
           <p>From <b>${props.price}</b>/person</p>
        </div>
    )
}