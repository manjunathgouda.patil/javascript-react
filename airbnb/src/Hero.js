import React from "react"
import image from "./asset/Group77.jpg"
export default function Hero()
{
  return(
  <section className="Hero">
    <img src={image} className="Heropic"/>
    <h1 className="Hero-heading">Online Experiences </h1>
    <p className="Hero-text">Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.
</p>
    </section>
  )
}