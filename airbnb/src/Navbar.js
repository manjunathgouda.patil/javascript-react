import React from "react"
import airbnb from "./asset/airbnb.jpg"
export default function Navbar()
{
    return (
        <nav className="Navbar">
            <img src={airbnb} className="logo"/>
        </nav>
    )
}