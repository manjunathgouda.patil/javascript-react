export default[
    {
        id:1,
        title:"Life Lessons with Katie Zaferes",
        price:136,
        rating:5.0,
        cty:"USA"

    },
    {
        id:2,
        title:"Learn Wedding Photography",
        price:125,
        rating:5.0,
        cty:"USA"
    },
    {
        id:3,
        title:"Group Mountain Biking",
        price:50,
        rating:4.8,
        cty:"USA"
    }
]