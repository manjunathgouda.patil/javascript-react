import ReactDOM from "react-dom"
import React from "react"
//import meme from "./Meme.js"
import memedata from "./memesData.js"


export default function Form()
{
    const [memeImage,setmemeImage]=React.useState(
        {
            topText:"",
            bottomText:"",
            image:"https://i.imgflip.com/30b1gx.jpg"
        }
    )
    
 function getMemeImage()
 {
     const memeArray = memedata.data.memes
     const randomNumber=Math.floor(Math.random()*memeArray.length)
     setmemeImage(prevImg=>({
         ...prevImg,
          image:memeArray[randomNumber].url
     }))
        
    // console.log(randomNumber)
 }   

    return(
        <div className="forms">
        <div className="input">
        <input type="text"/>
        <input type="text"/>
        </div>
        <button className="button" onClick={getMemeImage}>Get a new meme image  🖼</button>
        <img src={memeImage.image} className="img--meme"/>
        </div>
        
         )
}
