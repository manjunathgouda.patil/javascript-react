import React from "react"
import ReactDOM from "react-dom"
import Header from "./Header.js"
import Form from "./Form.js"
function Index(){
    return(
        <div>
            <Header/>
            <Form/>
        </div>
    )
}

ReactDOM.render(<Index />,document.getElementById("root"));